"""
.. module:: main
   :platform: Unix
   :synopsis: Prototype

.. moduleauthor:: Fredrik Nysjo

"""

import linalg

import sys
import time
import serial
import signal
import binascii
import struct

import mmap


# Commands obtained via serial port monitoring.

TOUCH_CMD_INIT = bytearray([0xa5, 0x5a, 0x04, 0x3c, 0x00, 0x00, 0x00, 0x00,
    0xfe, 0xff, 0xff, 0xff, 0x1c, 0x00, 0x00, 0x00, 0x00, 0xf4, 0x01, 0x00,
    0x11, 0x10, 0x00, 0x00, 0x00, 0x00, 0x64, 0x00, 0x64, 0x00, 0x08, 0x00,
    0x00, 0x11, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x43, 0x4f, 0x4d, 0x36])

TOUCH_CMD_INPUT_WARMUP = bytearray([0xa5, 0x5a, 0x03, 0x0c, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

TOUCH_CMD_INPUT_NOFORCE = bytearray([0xa5, 0x5a, 0x03, 0x0c, 0x00, 0x08, 0x00,
    0x08, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

TOUCH_CMD_UNKNOWN_0 = bytearray([0xa5, 0x5a, 0x07, 0x07, 0x01, 0x00, 0x00,
    0xfa, 0x01, 0x00, 0x0a])  # Turns on blue LED (pulsing).

TOUCH_CMD_UNKNOWN_1 = bytearray([0xa5, 0x5a, 0x07, 0x07, 0x01, 0x80, 0x80,
    0x80, 0x00, 0x00, 0x00])  # Turns on white LED.

TOUCH_CMD_UNKNOWN_2 = bytearray([0xa5, 0x5a, 0x0c, 0x01, 0x00])

TOUCH_CMD_SHUTDOWN = bytearray([0xa5, 0x5a, 0x03, 0x0c, 0x00, 0x08, 0x00,
    0x08, 0x00, 0x08, 0x00, 0x00, 0xb2, 0x05, 0x00, 0x00])


# Values (scaling, offset) for converting raw encoder values to degrees.
# Obtained mostly by guessing...
RAW_TO_DEGREES = [(0.02994, 0.0), (-0.02994, 0.0), (0.02994, 0.0),
    (-0.05694, -2040.0), (-0.05694, -2040.0), (-0.05694, -2040.0)]


# Measured link lengths of device. May be some errors here...
LINK_LENGTHS = [0.175, 0.120]


class TouchData(object):
    encoders_raw = [0, 0, 0, 0, 0, 0]  # X Y Z GX GY GZ
    encoders_degrees = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  # X Y Z GX GY GZ
    inkwell = False
    buttons = 0  # Bit 1 = main button; bit 2 = secondary button.
    position = [0.0, 0.0, 0.0]
    orientation = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
    timestamp = 0


class SharedData(object):
    def __init__(self, tagname):
        if sys.platform.startswith('linux'):
            self.f = open('/dev/shm/' + tagname, 'w+b') 
            self.f.write('123')  # Just to make sure that the file is not empty.
            self.f.flush()
            self.seg = mmap.mmap(self.f.fileno(), 0, access=mmap.ACCESS_WRITE)
        if sys.platform.startswith('win32'):
            self.seg = mmap.mmap(0, 256, tagname)

    def write(self, touch_data):
        buf = bytearray(256)
        struct.pack_into('i', buf, 0, touch_data.timestamp)
        struct.pack_into('fff', buf, 4, *touch_data.position)
        struct.pack_into('fffffffff', buf, 4 * 4, *touch_data.orientation)
        struct.pack_into('i', buf, 13 * 4, touch_data.buttons)
        struct.pack_into('i', buf, 14 * 4, touch_data.timestamp)
        if self.seg.size() < len(buf):
            self.seg.resize(len(buf))
        self.seg[0:len(buf)] = str(buf)


is_alive = True
def shutdown_handler(signum, frame):
    global is_alive
    is_alive = False


class PushingWindowFilter(object):
    """Simple input filter using a deadband and a pushing window"""

    def __init__(self, deadband, center=0):
        self.deadband = deadband
        self.center = center

    def update(self, x):
        diff = x - self.center
        if diff <= -self.deadband:
            self.center -= abs(diff) - self.deadband
        elif diff > self.deadband:
            self.center += abs(diff) - self.deadband
        return self.center


def reconstruct_pose(encoders_degrees, offset_matrix=None):
    """Reconstructs stylus pose using forward kinematics"""
    x = encoders_degrees[0]
    y = encoders_degrees[1]
    z = encoders_degrees[2]
    gx = encoders_degrees[3]
    gy = encoders_degrees[4]
    gz = encoders_degrees[5]

    # Define joint rotations and link translations in right handed system.
    base_rotation = linalg.rotate(-x, 0.0, 1.0, 0.0)
    joint0 = linalg.rotate(-y, 1.0, 0.0, 0.0)
    link01 = linalg.translate(0.0, 0.0, LINK_LENGTHS[0])
    joint1 = linalg.rotate(-(z - y - 20.0), 1.0, 0.0, 0.0)
    link12 = linalg.translate(0.0, -LINK_LENGTHS[1], 0)
    joint2 = (linalg.rotate(-gx, 0.0, 1.0, 0.0) *
             linalg.rotate(gy, 1.0, 0.0, 0.0) *
             linalg.rotate(-gz, 0.0, 0.0, 1.0))

    pose = base_rotation * joint0 * link01 * joint1 * link12 * joint2
    if offset_matrix != None:
        pose = offset_matrix * pose
    return pose


def process(response, input_filters):
    """Processes raw data received from device"""
    assert(len(response) == 32)

    data = TouchData()

    data.encoders_raw[0] = struct.unpack_from('h', response, 4)[0]
    data.encoders_raw[1] = struct.unpack_from('h', response, 6)[0]
    data.encoders_raw[2] = struct.unpack_from('h', response, 8)[0]
    data.encoders_raw[3] = struct.unpack_from('h', response, 10)[0]
    data.encoders_raw[4] = struct.unpack_from('h', response, 12)[0]
    data.encoders_raw[5] = struct.unpack_from('h', response, 14)[0]
    data.inkwell = True if struct.unpack_from('b', response, 16)[0] == 0 else False
    data.buttons = 0
    data.buttons |= 0x1 if struct.unpack_from('b', response, 18)[0] == 0 else 0x0
    data.buttons |= 0x2 if struct.unpack_from('b', response, 20)[0] == 0 else 0x0
    data.timestamp = struct.unpack_from('i', response, 24)[0]

    if input_filters == []:
        deadbands = [0, 0, 0, 7, 7, 7]
        input_filters.extend([PushingWindowFilter(x, y) for x, y in zip(
            deadbands, data.encoders_raw)])
    for i in xrange(0, 6):
        data.encoders_raw[i] = input_filters[i].update(data.encoders_raw[i])

    data.encoders_degrees = [(c[0] * (x + c[1]))
        for x, c in zip(data.encoders_raw, RAW_TO_DEGREES)]

    pose_offset = linalg.translate(0.0, 0.1143, -0.1351)  # TODO
    pose = reconstruct_pose(data.encoders_degrees, pose_offset)
    data.position = [x for x in linalg.translation_part(pose).flat]
    data.orientation = [x for x in linalg.rotation_part(pose).flat]

    return data


def main():
    # Set up signal handler to allow clean shutdown.
    global is_alive
    signal.signal(signal.SIGINT, shutdown_handler)

    # Platform specific settings.
    if sys.platform.startswith('linux'):
        #portname = '/dev/ttyACM0'
        portname = '/dev/3dstylus'  # Requires udev rule.
        baudrate = 115200  # 128000 gives "Failed to set custom baud rate" error.
    elif sys.platform.startswith('win32'):  # Untested.
        portname = 'COM6'
        baudrate = 128000
    else:
        sys.exit('Unsupported platform')

    # Create shared memory segment for writing data to.
    shared_data = SharedData('touch_3d')
    
    # Open serial port. Reads and writes should be non-blocking and
    # blocking, respectively.
    print('Starting up...')
    port = serial.Serial(portname, baudrate, bytesize=serial.EIGHTBITS,
        parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
        timeout=0, xonxoff=False, rtscts=True, writeTimeout=None, dsrdtr=True)
    if (port == None):
        sys.exit('Could not open port')

    # Initialize device.
    port.write(TOUCH_CMD_INIT)

    # Start feedback loop.
    input_filters = []
    timestamp = 0
    port.write(TOUCH_CMD_INPUT_WARMUP)
    while is_alive == True:
        # Poll device for response.
        response = bytes(port.read(32))
        while len(response) == 0:
            response = bytes(port.read(32)) 
        if len(response) == 32:
            data = process(response, input_filters)
            shared_data.write(data)

        # Process response.
        if timestamp != 0 and timestamp % 100 == 0: 
            if len(response) == 32:
                print(data.encoders_raw, data.inkwell, data.buttons)
                #print(data.encoders_degrees)
                print(data.position)
                print(data.orientation)
            #print(binascii.hexlify(data))

        # Send next input command.
        timestamp += 1
        if timestamp < 5: 
            cmd = bytearray(TOUCH_CMD_INPUT_WARMUP)
        else:
            cmd = bytearray(TOUCH_CMD_INPUT_NOFORCE)
        cmd[12] = (timestamp >> 0) & 0xff
        cmd[13] = (timestamp >> 8) & 0xff
        cmd[14] = (timestamp >> 16) & 0xff
        cmd[15] = (timestamp >> 24) & 0xff
        port.write(cmd)
        time.sleep(1e-3)

    print('Shutting down...')
    port.write(TOUCH_CMD_SHUTDOWN)
    port.close()

    sys.exit()


if __name__ == '__main__':
    main()
