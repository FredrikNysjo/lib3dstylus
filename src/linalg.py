"""
.. module:: linalg
   :platform: Unix
   :synopsis: Helper function for matrices and vectors.

.. moduleauthor:: Fredrik Nysjo

"""

import math
import numpy


def vec4(x, y, z, w=1.0):
    """Returns 4D float vector"""
    return numpy.matrix([x, y, z, w], numpy.float32).transpose()


def mat4(m00=1.0, m01=0.0, m02=0.0, m03=0.0,
    m10=0.0, m11=1.0, m12=0.0, m13=0.0,
    m20=0.0, m21=0.0, m22=1.0, m23=0.0,
    m30=0.0, m31=0.0, m32=0.0, m33=1.0):
    """Return 4x4 float matrix"""
    return numpy.matrix([[m00, m01, m02, m03],
        [m10, m11, m12, m13],
        [m20, m21, m22, m23],
        [m30, m31, m32, m33]], numpy.float32)


def translate(tx, ty, tz):
    """Returns 4x4 translation matrix"""
    return mat4(1.0, 0.0, 0.0, tx,
        0.0, 1.0, 0.0, ty,
        0.0, 0.0, 1.0, tz,
        0.0, 0.0, 0.0, 1.0)


def rotate(angle_degrees, x, y, z):
    """Returns 4x4 rotation matrix"""
    theta = angle_degrees * (math.pi / 180.0)
    a = math.cos(theta)
    b = math.sin(theta)
    c = 1.0 - a
    return mat4((a + x**2 * c), (x * y * c - z * b), (x * z * c + y * b), 0.0,
        (x * y * c + z * b), (a + y**2 * c), (y * z * c - x * b), 0.0,
        (x * z * c - y * b), (y * z * c + x * b), (a + z**2 * c), 0.0,
        0.0, 0.0, 0.0, 1.0)


def translation_part(matrix):
    """Returns translation part of 4x4 matrix"""
    assert(matrix.shape == (4, 4))
    return matrix[0:3, 3]


def rotation_part(matrix):
    """Returns 3x3 rotation part of 4x4 matrix"""
    assert(matrix.shape == (4, 4))
    return matrix[0:3, 0:3]
