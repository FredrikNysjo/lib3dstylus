a5  # Header (4 bytes)
5a  #
01  #
1c  #
be  # X encoder (2 bytes)
06  #
46  # Y encoder (2 bytes)
00  #
0b  # Z encoder (2 bytes)
fc  #
59  # Gimbal X encoder (2 bytes)
07  #
a5  # Gimbal Y encoder (2 bytes)
0b  #
ca  # Gimbal Z encoder (2 bytes)
07  #
00  # Inkwell (0=On; 1=Off)
00  # ?
01  # Button 0 (0=On; 1=Off)
00  # ?
01  # Button 1 (0=On; 1=Off)
00  # ?
00  # ?
00  # ?
98  # Timestamp (4 bytes)
21  #
00  #
00  #
aa  # ?
aa  # ?
00  # ?
00  # ?
